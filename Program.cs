﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Models;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System.Threading;

namespace AggregatorComponent
{
    class Program
    {
        static TimerFactory TimeDat = new TimerFactory();
        static Dictionary<string, initPacketDataModel> sessionStore = new Dictionary<string, initPacketDataModel>();
        static Dictionary<string, List<MSReplyModel>> messageStore = new Dictionary<string, List<MSReplyModel>>();            

        static void Main(string[] args)
        {
            var HostName = "dropletrabbit";
            var factory = new ConnectionFactory(){HostName=HostName};
            
            factory.Uri = new Uri(Environment.GetEnvironmentVariable("RABBIT_URI"), UriKind.Absolute);
            
            using (var connection = factory.CreateConnection())
            {
                using (var channel = connection.CreateModel())
                {
                    var ended = new ManualResetEventSlim();
                    channel.QueueDeclare("msres_queue",false,false,false,null);
                    channel.ExchangeDeclare("msres_ex",ExchangeType.Direct);
                    channel.QueueBind("msres_queue","msres_ex","");

                    channel.QueueDeclare("aggres_queue",false,false,false,null);
                    channel.ExchangeDeclare("aggres_ex",ExchangeType.Direct);
                    channel.QueueBind("aggres_queue","aggres_ex","");


                    var msConsumer = new EventingBasicConsumer(channel);
                    msConsumer.Received += (model,ea) => {
                        var message = Encoding.UTF8.GetString(ea.Body);

                        System.Console.WriteLine("----------------------------------------------------------------");
                        System.Console.WriteLine("MS RECEIVED : " + message);

                        var data = JsonConvert.DeserializeObject<MSReplyModel>(message);
                        // message is sent outside timing-window
                        if (data.request_type == "reservation"){
                            var newAggRes = new AggResModelV2(){type=data.type, status=data.status, description=data.description, additional_info=data.additional_info, eventid=data.eventid, sessionid= sessionStore[data.eventid].sessionId, request_type=data.request_type};
                            var rabbitMesssage = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(newAggRes));
                            channel.BasicPublish("aggres_ex", "", body:rabbitMesssage);
                            System.Console.WriteLine("-----------------------------------------------------");
                            System.Console.WriteLine($"AGG SENDING : {JsonConvert.SerializeObject(newAggRes)}");

                        }
                        if(data.request_type == "confirm"){
                            storeMessage(data.eventid, data); 
                        }

                    };


                    var InitPacketconsumer = new EventingBasicConsumer(channel);
                    InitPacketconsumer.Received += (model,ea) =>{
                        var message = Encoding.UTF8.GetString(ea.Body);

                        System.Console.WriteLine("----------------------------------------------------------------");
                        System.Console.WriteLine("Init packet - RECEIVED : " + message);

                        var initPack = JsonConvert.DeserializeObject<InitPacketModel>(message);
                        var startTimerFlag = false;
                        if (initPack.request_type == "confirm"){
                            startTimerFlag = true;
                        }
                        StoreSessionId(initPack.eventid, initPack.sessionid, initPack.request_type, startTimerFlag);    
                    };

                    TimeDat.TimerExpiredEvent += (tea) => {
                        System.Console.WriteLine("-------------------------------------------");
                        System.Console.WriteLine("ELAPSED TIMER FOR KEY " + tea.eventid);
                        var objDataList = messageStore[tea.eventid];
                        var sendModel = new AggResModel();
                        sendModel.eventid=tea.eventid;
                        sendModel.request_type = sessionStore[tea.eventid].request_type;
                        sendModel.sessionid = sessionStore[tea.eventid].sessionId;
                        sendModel.status = new List<ListData>();
                        foreach (var item in objDataList){
                            var tmp = new ListData(){status=item.status, description=item.description, type=item.type, additional_info=item.additional_info};
                            sendModel.status.Add(tmp);
                        }
                        System.Console.WriteLine("AGG OUTPUT : " + JsonConvert.SerializeObject(sendModel));
                        //convert and send to via rabbitmq
                        var rabbitMesssage = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(sendModel));
                        channel.BasicPublish("aggres_ex", "", body:rabbitMesssage);

                    };

                    channel.BasicConsume("Agr_initPacket_queue",true,InitPacketconsumer);
                    channel.BasicConsume("msres_queue",true, msConsumer);

                    System.Console.WriteLine("Aggregator running...");
                    ended.Wait();
                    System.Console.WriteLine("Shutting down.");
                }
            }
        }

        private static void StoreSessionId(string key, string sesid, string reqtype, bool startTimer){
            var data = new initPacketDataModel(){sessionId=sesid, request_type=reqtype};

            try
            {
                if (sessionStore.ContainsKey(key)){
                    sessionStore[key]=data;
                }else{
                    sessionStore.Add(key, data);
                }
                
            }
            catch (System.Exception ex)
            {
                System.Console.WriteLine(ex.Message);

            }
            if(startTimer){
                System.Console.WriteLine("TIMER STARTED FOR KEY : " + key);
                Task.Run(()=>TimeDat.CreateTimer(key, 10000));                
            }
        }

        private static void storeMessage(string key, MSReplyModel msData){
            if(!messageStore.ContainsKey(key)){
                messageStore.Add(key, new List<MSReplyModel>());
                messageStore[key].Add(msData);
            }else{
                messageStore[key].Add(msData);
            }
        }
    }
}
