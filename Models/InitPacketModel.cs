namespace Models
{
    public class InitPacketModel
    {
        public string eventid { get; set; }
        public string sessionid { get; set; }
        public string request_type { get; set; }
    }
}