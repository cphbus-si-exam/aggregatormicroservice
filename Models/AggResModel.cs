using System.Collections.Generic;

namespace Models
{
    public class AggResModel
    {
        public List<ListData> status { get; set; }
        public string eventid { get; set; }
        public string sessionid { get; set; }
        public string request_type { get; set; }

    }


    public class AggResModelV2
    {
        public string type { get; set; }
        public bool status { get; set; }
        public string description { get; set; }
        public string additional_info { get; set; }
        public string eventid { get; set; }
        public string sessionid { get; set; }
        public string request_type { get; set; }
    }



    public class ListData
    {
        public string type { get; set; }
        public bool status { get; set; }
        public string description { get; set; }
        public string additional_info { get; set; }
    }
}